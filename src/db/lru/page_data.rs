use super::*;
#[derive(Clone, Copy, Debug, Deserialize, Display, FromStr, Serialize)]
#[serde(rename_all = "kebab-case")]
#[enumeration(rename_all= = "kebab-case")]
enum DataUse {
	#[display(fmt = "data")]
	Data,
	#[display(fmt = "data-saver")]
	DataSaver,
}
#[derive(Clone, Debug, Deserialize)]
pub struct PageData {
	token: Option<String>,
	#[serde(flatten)]
	pub page_info: PageInfo,
	// garbage data
	_g: Option<String>,
}
#[derive(Clone, Debug, Deserialize)]
pub struct PageInfo {
	size: DataUse,
	#[serde(with = "hex")]
	hash: [u8; 16],
	idx: NonZeroU16,
	prefix: Prefix,
	#[serde(deserialize_with = "serde_strz::deserialize")]
	ext: Ext,
}
impl PageData {
	pub fn token(&self) -> Option<&String> {
		self.token.as_ref()
	}

	pub fn key(&self) -> Key {
		Key {
			hash: self.hash,
			idx: self.idx,
			prefix: self.prefix,
			ext: self.ext,
		}
	}

	pub fn to_key_val(&self) -> (Key, (u8, usize, Ext)) {
		(
			self.key(),
			(self.prefix.into(), self.idx.get() as usize - 1, self.ext),
		)
	}
}
impl fmt::Display for PageData {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		let &PageInfo {
			hash,
			idx,
			prefix,
			ext,
			..
		} = &self.page_info;
		let prefix = prefix.into();
		let prefix: &[u8] = slice::from_ref(&prefix);
		let prefix = unsafe { str::from_utf8_unchecked(prefix) };
		if self.token.is_some() {
			f.write_str("/{token}")?;
		}
		write!(f, "/{}/{}{}.{}", encode(hash), prefix, idx, ext.as_ext())?;
		if self._g.is_some() {
			f.write_str("{garbage}/{garbage}/{garbage}/{garbage}")
		} else {
			Ok(())
		}
	}
}
#[derive(Debug, Display, From)]
enum PageDataErr {
	FromHex(::hex::FromHexError),
	#[display(fmt = "Found none, expected some")]
	None,
	#[display(fmt = "Found some, expected none")]
	Some,
	#[display(fmt = "Missing prefix")]
	MissingPrefix,
	#[display(fmt = "Bad int: {}", _0)]
	Int(::std::num::ParseIntError),
	#[display(fmt = "Bad extension")]
	Ext,
}
// impl str::FromStr for PageData {
// 	type Err = PageDataErr;
//
// 	fn from_str(s: &str) -> Result<Self, Self::Err> {
// 		let mut i = s.splitn("/", 1usize);
// 		let hash = i.next().or_else(PageDataErr::None)?;
// 		let hash = FromHex::from_hex(hash)?;
// 		let page = i.next().or_else(PageDataErr::None)?;
// 		if i.next().is_some() {
// 			return Err(PageDataErr::Some);
// 		}
// 		let (prefix, page) = page.split_at(1);
// 		let prefix = prefix.bytes().next().or_else(PageDataErr::MissingPrefix)?;
// 		let prefix = Prefix(unsafe { NonZeroU8::new_unchecked(prefix) });
// 		let mut i = page.splitn(".", 1);
// 		let idx = i.next().or_else(PageDataErr::None)?.parse()?;
// 		let ext = i
// 			.next()
// 			.or_else(PageDataErr::None)?
// 			.parse()
// 			.map_err(|()| PageDataErr::Ext)?;
// 		if i.next().is_some() {
// 			return Err(PageDataErr::Some);
// 		}
// 		Ok(Self {
// 			data_use: DataUse::Data,
// 			token: None,
// 			hash,
// 			prefix,
// 			idx,
// 			ext,
// 			garbage: None,
// 		})
// 	}
// }
