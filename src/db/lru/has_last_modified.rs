use actix_web::{
	dev::Payload,
	FromRequest,
	HttpRequest,
};
use derive_more::{
	Deref,
	DerefMut,
	Into,
};
use std::{
	convert::Infallible,
	future::Future,
	pin::Pin,
	task::{
		Context,
		Poll,
	},
};
#[derive(Clone, Copy, Default, Deref, DerefMut, Into)]
pub struct HasLastModified(bool);
impl HasLastModified {
	pub fn has_last_modified(&self) -> bool {
		self.0
	}
}
#[derive(Copy, Clone, Default, Deref, DerefMut, Into)]
pub struct AlwaysServeFull(bool);
impl FromRequest for HasLastModified {
	type Config = AlwaysServeFull;
	type Error = Infallible;
	type Future = Self;

	fn from_request(
		req: &HttpRequest,
		_payload: &mut Payload,
	) -> Self::Future {
		Self::extract(req)
	}

	fn extract(req: &HttpRequest) -> Self::Future {
		let nvm: Self::Config = req.app_data().copied().unwrap_or_default();
		if nvm.into() {
			Self(false)
		} else {
			let lm = req.headers().contains_key("last-modified");
			Self(lm)
		}
	}
}
impl Future for HasLastModified {
	type Output = Result<Self, Infallible>;

	fn poll(
		self: Pin<&mut Self>,
		_cx: &mut Context,
	) -> Poll<Self::Output> {
		let value = self.into_ref().as_ref().0;
		Poll::Ready(Ok(Self(value)))
	}
}
