use crate::req::*;
use actix_web::web::{
	Data,
	HttpRequest,
	HttpResponse,
};
use ahash::RandomState;
use derive_more::{
	From,
	Into,
};
use lru_disk_cache::lru_cache::{
	CountableMeterWithMeasure,
	LruCache,
	Meter,
};
use pinecone::{
	take_from_bytes,
	to_vec,
};
use serde::{
	Deserialize,
	Serialize,
};
use std::{
	fs,
	io::{
		self,
		Read,
		Write,
	},
	num::NonZeroU16,
	str,
	sync::Mutex,
};

type Key = PageInfo;
mod val;

#[derive(Clone, Copy, Debug)]
pub struct FileSize {
	round_to_high_bits: u64,
}
type Val = Page;
type Alru = LruCache<Key, Val, RandomState, FileSize>;
mod has_last_modified;
use has_last_modified::HasLastModified;
#[derive(Into)]
struct KV(Key, Val);
impl From<&(PageData, u64)> for KV {
	fn from((page, size): &(PageData, u64)) -> Self {
		let key = Key {
			size: page.info.size,
			hash: page.info.hash,
			prefix: page.info.prefix,
			idx: page.info.idx,
			ext: page.info.ext,
		};
		let val = Val {
			ext_size: page.info.ext.to_bits(*size),
			#[cfg(feature = "file-hash")]
			file_hash: page._file_hash.unwrap_or_default(),
		};
		KV(key, val)
	}
}

#[derive(
	Clone, Deserialize, Eq, From, Into, Ord, PartialEq, PartialOrd, Serialize,
)]
pub struct SerdeKV {
	hash: [u8; 16],
	prefix: Prefix,
	#[serde(with = "val")]
	pages: Vec<Option<Page>>,
}
pub struct SerdeKVIter {
	hash: [u8; 16],
	prefix: Prefix,
	pages: ::std::iter::Enumerate<::std::vec::IntoIter<Option<Page>>>,
}
//  = FnMut((usize, Option<Page>)) -> (Key, u32)
impl Iterator for SerdeKVIter {
	type Item = (Key, Val);

	fn next(&mut self) -> Option<Self::Item> {
		let (idx, page) = loop {
			let op = self.pages.next()?;
			if let (idx, Some(pg)) = op {
				break (idx, pg);
			}
		};
		let (ext, size) = Page::to_ext_size(page);
		let idx = NonZeroU16::new(idx as u16)?;
		let key = Key {
			hash: self.hash,
			prefix: self.prefix,
			idx,
			ext,
		};
		Some((key, page))
	}
}
impl IntoIterator for SerdeKV {
	type IntoIter = SerdeKVIter;
	type Item = (Key, Val);

	fn into_iter(self) -> Self::IntoIter {
		let Self {
			hash,
			prefix,
			pages,
		} = self;
		SerdeKVIter {
			hash,
			prefix,
			pages: pages.into_iter().enumerate(),
		}
	}
}
impl CountableMeterWithMeasure<Key, Val, u64> for FileSize {
	fn meter_add(
		&self,
		curr: u64,
		amt: u64,
	) -> u64 {
		curr + amt
	}

	fn meter_sub(
		&self,
		curr: u64,
		amt: u64,
	) -> u64 {
		curr - amt
	}

	fn meter_size(
		&self,
		curr: u64,
	) -> Option<u64> {
		Some(curr)
	}
}
impl Meter<Key, Val> for FileSize {
	type Measure = u64;

	fn measure<Q: ?Sized>(
		&self,
		_: &Q,
		val: &Val,
	) -> Self::Measure {
		let (_, size) = (*val).to_ext_size();
		(size as u64 | self.round_to_high_bits) + 1
	}
}

pub fn setup_cache(args: &crate::Args) -> io::Result<Data<Mutex<Alru>>> {
	let meta_dir = args.cache_store.join("meta");
	let cache_file = meta_dir.join("cache.bin");
	let meter = FileSize {
		round_to_high_bits: args.file_allocation_size.to_bytes() - 1,
	};
	let hash = RandomState::new();
	let cap = args.max_disk_usage.to_bytes();
	let mut alru = Alru::with_meter_and_hasher(cap, meter, hash);
	match fs::OpenOptions::new().read(true).open(&cache_file) {
		Err(e) if e.kind() == io::ErrorKind::NotFound => {
			fs::create_dir_all(&meta_dir).expect("meta dir");
			let cache_data: Vec<SerdeKV> = vec![];
			let slic = to_vec(&cache_data).expect("sorry whut");
			let mut file = fs::File::create(&cache_file)?;
			file.write(&slic)?;
			file.flush()?;
		}
		Ok(mut file) => {
			let len = file.metadata()?.len();
			let mut data = Vec::with_capacity(len as usize);
			assert_eq!(
				file.read_to_end(&mut data)? as u64,
				len,
				"File changed while we were reading it"
			);
			let (meta, ex): (Vec<SerdeKV>, &[u8]) =
				take_from_bytes(&data).expect("whut");
			assert_eq!(ex.len(), 0, "File had excess data");
			alru.extend(meta.into_iter().flatten())
		}
		Err(e) => return Err(e),
	};
	Ok(Data::new(Mutex::new(alru)))
}
// #[get("/{size}/{hash}/{prefix:[a-zA-Z]}{idx:[1-9][0-9]*}.{ext}")]
// #[get("/{token}/{size}/{hash}/{prefix:[a-zA-Z]}{idx:[1-9][0-9]*}.{ext}")]
// #[get("/{token}/{size}/{hash}/{prefix:[a-zA-Z]}{idx:[1-9][0-9]*}.{ext}/{_g:.*}")]
pub async fn serve_file(req: HttpRequest) -> HttpResponse {
	let alru: Data<Mutex<Alru>> = match req.app_data().cloned() {
		Some(r) => r,
		None => return HttpResponse::InternalServerError().finish(),
	};
	let lmod: HasLastModified = match req.extensions().get() {
		Some(&r) => r,
		None => return HttpResponse::InternalServerError().finish(),
	};
	let page: PageData = {
		use actix_router::PathDeserializer;
		let path = req.path();
		let de = PathDeserializer::new(req.match_info());
		match Deserialize::deserialize(de) {
			Ok(page) => page,
			Err(_) => return HttpResponse::BadRequest().finish(),
		}
	};
	if lmod.has_last_modified() {
		HttpResponse::NotModified().finish()
	} else {
		match alru.lock().unwrap().get(&page.key()) {
			Some(val) => {}
			None => {}
		}
		HttpResponse::NoContent().finish()
	}
}
