use super::Page;
use serde::{
	de::*,
	ser::*,
};
use std::{
	fmt,
	num::NonZeroU32,
};
type Pages = Vec<Option<Page>>;
pub fn serialize<S>(
	v: &Pages,
	s: S,
) -> Result<S::Ok, S::Error>
where
	S: Serializer,
{
	let mut seq = s.serialize_seq(v.len().into())?;
	for el in v {
		seq.serialize_element(&Page::to_u32(*el))?;
	}
	seq.end()
}
struct PagesVisitor;
impl<'de> Visitor<'de> for PagesVisitor {
	type Value = Pages;

	fn expecting(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		f.write_str("a list of page descriptors")
	}

	fn visit_seq<A>(
		self,
		mut seq: A,
	) -> Result<Self::Value, A::Error>
	where
		A: SeqAccess<'de>,
	{
		let len = seq.size_hint().unwrap_or(16);
		let mut sv = Vec::with_capacity(len);
		while let Some(value) = seq.next_element()? {
			let op = NonZeroU32::new(value).map(Page::new);
			sv.push(op);
		}
		Ok(sv)
	}
}
pub fn deserialize<'de, D>(d: D) -> Result<Pages, D::Error>
where
	D: Deserializer<'de>,
{
	d.deserialize_seq(PagesVisitor)
}
