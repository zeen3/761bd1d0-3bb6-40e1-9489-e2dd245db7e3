use std::sync::atomic::AtomicU64 as I;
#[derive(Debug, Default, serde::Serialize, serde::Deserialize)]
pub struct Stats {
	#[serde(rename = "requests_served")]
	req_served: I,
	cache_hits: I,
	#[serde(rename = "cache_misses")]
	cache_miss: I,
	#[serde(rename = "browser_cached")]
	brow_cache: I,
	bytes_sent: I,
	#[serde(rename = "bytes_on_disk")]
	bytes_disk: I,
}
