use super::TlsCert;
use derive_more::Constructor;
use serde::Serialize;
use std::sync::atomic::Ordering::Relaxed;
async fn mb<T: serde::de::DeserializeOwned>(
	mut req: awc::ClientResponse
) -> Result<T, crate::Error> {
	match req.status().as_u16() {
		200 | 202 => {
			// wait for it
			let data = req.json().await?;
			Ok(data)
		}
		415 => {
			// Stop always sends json
			Err(crate::Error::Unknown)
		}
		400 => {
			// badly formed json (also technically impossible)
			Err(crate::Error::Unknown)
		}
		401 => {
			// invalid secret
			Err(crate::Error::InvalidSecret)
		}
		500.. => Err(crate::Error::ControlServer),
		_ => unreachable!("all possible responses used"),
	}
}
/// Ping to server
///
/// Used for ensuring it's known to be awake
#[derive(Debug, Serialize)]
pub struct Ping {
	secret: crate::ClientSecret,
	port: u16,
	disk_space: u64,
	network_speed: u64,
	build_version: u16,
	/// if None, we need a cert; if Some, we dont.
	#[serde(skip_serializing_if = "Option::is_none")]
	tls_created_at: Option<crate::Rfc3339>,
}
impl Ping {
	pub fn new<TLS>(
		a: &crate::Args,
		tls: TLS,
	) -> Self
	where
		TLS: Into<Option<TlsCert>>,
	{
		let port = {
			let ext = a.external_port.load(Relaxed);
			if ext == 0 { a.bind.port() } else { ext }
		};
		Self {
			secret: a.client_secret.expect("a client secret"),
			port,
			disk_space: a.max_disk_usage.to_bytes(),
			network_speed: a.max_transfer_short.per_sec(),
			build_version: crate::BUILD_VERSION,
			tls_created_at: tls.into().map(|tls| tls.created_at),
		}
	}

	pub async fn send(
		&mut self,
		client: crate::Client,
		server: &crate::Server,
	) -> Result<super::ServerSettings, crate::Error> {
		let data = client
			.post(format!("{}/ping", server))
			.send_json(self)
			.await?;

		let settings: super::ServerSettings = mb(data).await?;
		if let Some(created_at) = settings.tls.as_ref().map(|tls| tls.created_at) {
			self.tls_created_at = Some(created_at);
		}
		Ok(settings)
	}
}

/// Stop
///
/// Sent to stop recieving any more requests from clients
#[derive(Debug, Serialize, Constructor)]
pub struct Stop {
	secret: crate::ClientSecret,
}
impl Stop {
	pub async fn send(
		&mut self,
		client: crate::Client,
		server: &crate::Server,
	) -> Result<(), crate::Error> {
		let data = client
			.post(format!("{}/stop", server))
			.send_json(self)
			.await?;

		mb(data).await
	}
}
