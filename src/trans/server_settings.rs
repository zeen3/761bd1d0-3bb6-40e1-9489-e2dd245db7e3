use serde::Deserialize;
use std::fmt::{
	self,
	Debug,
};
use url::Url;
/// Recieved from the main server when sending a ping
#[derive(Debug, Deserialize, PartialEq, Eq, Clone)]
pub struct ServerSettings {
	/// Server to collect images from
	pub image_server: Url,
	/// Latest build of official client/latest API version
	pub latest_build: u32,
	/// Full URL of client
	pub url: Url,
	/// Key used as shared key for token decoding
	#[serde(with = "crate::b64")]
	pub token_key: Vec<u8>,
	pub compromised: bool,
	pub paused: bool,
	pub force_tokens: bool,
	pub tls: Option<TlsCert>,
	#[serde(default = "allowed_referers")]
	pub allowed_referers: Vec<String>,
	#[serde(default = "permit_blank_referer")]
	pub permit_blank_referer: bool,
}
fn allowed_referers() -> Vec<String> {
	vec!["mangadex.org".into(), "mangadex.network".into()]
}
const fn permit_blank_referer() -> bool {
	true
}

/// May be recieved from the main server when sending a ping
#[derive(Deserialize, PartialEq, Eq, Clone)]
pub struct TlsCert {
	pub created_at: crate::Rfc3339,
	pub(crate) private_key: String,
	pub(crate) certificate: String,
}
impl Debug for TlsCert {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		struct Private;
		impl Debug for Private {
			fn fmt(
				&self,
				f: &mut fmt::Formatter,
			) -> fmt::Result {
				f.write_str("****")
			}
		}
		f.debug_struct("TLSCert")
			.field("created_at", &self.created_at)
			.field("private_key", &Private)
			.field("certificate", &Private)
			.finish()
	}
}
