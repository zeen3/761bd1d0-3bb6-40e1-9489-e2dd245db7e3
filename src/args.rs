use crate::*;
use serde::{
	Deserialize,
	Serialize,
};
use std::{
	fs::{
		File,
		OpenOptions as Open,
	},
	io::{
		self,
		Read,
		Write,
	},
	net,
	path::PathBuf,
	sync::atomic::AtomicU16,
};

use structopt::StructOpt;
#[derive(Debug, StructOpt, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", default)]
pub struct Args {
	/// Client secret. Used to uniquely identify users.
	///
	/// Can be retrieved fron either https://mangadex-test.net,
	/// for staging only, or release at
	/// https://mangadex.org/md_at_home/request.
	#[structopt(long, env, hide_env_values = true)]
	#[serde(with = "serde_strz::emp")]
	pub client_secret: Option<ClientSecret>,
	/// Staging or Release server.
	#[structopt(long, short, env, default_value = "staging", parse(try_from_str = parse_server::from_str))]
	#[serde(with = "parse_server")]
	pub server: Server,
	/// Socket address
	#[structopt(long, short, env, default_value = "0.0.0.0:4430")]
	pub bind: net::SocketAddrV4,
	#[cfg(feature = "ipv6")]
	/// Socket address v6
	#[structopt(long, env, default_value = "[::]:4430")]
	pub bind_v6: net::SocketAddrV6,
	/// Max speed, over a short period
	///
	/// If a raw value is provided (ie: 1024), it
	/// will default to MiB / s
	#[structopt(long, env, default_value = "120 MiB / min")]
	#[serde(with = "serde_strz")]
	pub max_transfer_short: TransferPerPeriod,
	/// Max transfer, over a long period
	///
	/// If a raw value is provided (ie: 80), it
	/// will default to MiB / s
	#[structopt(long, env, default_value = "2 TiB / mo")]
	#[serde(with = "serde_strz")]
	pub max_transfer_long: TransferPerPeriod,
	/// Max disk usage
	///
	/// If no value is provided, uses the lower of
	/// 90% and 2 GiB free the total free space on
	/// the disk of the cache directory and the cache
	/// directory.
	#[structopt(long, env, default_value = "16 GiB")]
	#[serde(with = "serde_strz")]
	pub max_disk_usage: Size,
	/// Cache directory
	#[structopt(long, env, default_value = "cache")]
	pub cache_store: PathBuf,
	#[cfg(none)]
	/// Settings file
	#[structopt(long, default_value = "settings.toml")]
	pub settings_file: PathBuf,
	#[structopt(long, short, default_value = "60 s")]
	/// Time to wait to shutdown the server after a SIGINT
	pub graceful_shutdown: Period,
	#[structopt(
		long,
		default_value = "0",
		parse(try_from_str = parse_atomic_port)
	)]
	/// External port, if running behind a reverse proxy ie nginx
	///
	/// 0 is functionally a none-value
	pub external_port: AtomicU16,
	#[structopt(long, env)]
	/// Extra allowed origins, for self testing,
	///
	/// Preferable not to use, but still fine.
	pub extra_allowed_origins: Vec<String>,
	#[doc(hidden)]
	#[structopt(skip)]
	/// Data specific to staging server
	pub staging_data: Option<Box<StagingData>>,
	#[structopt(env, long, default_value = "4 KiB")]
	/// File allocation size
	///
	/// Internally this is used to find the maximum
	/// size we have available for real files.
	pub file_allocation_size: Size,
	/// Internal listeners
	#[serde(skip)]
	#[structopt(skip)]
	pub(crate) listeners: Listeners,
	/// Move faster and break more things.
	///
	/// See: A bit of [grease][] keeps the web moving.
	///
	/// Yes, this intentionally will break things.
	///
	/// [grease]: https://textslashplain.com/2020/05/18/a-bit-of-grease-keeps-the-web-moving/
	#[structopt(long)]
	pub enable_grease: bool,
}
lazy_static::lazy_static! {
	static ref SETTINGS_TOML: Args = {
		let toml = toml::toml! {
			client-secret = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
			server = "Staging"
			bind = "0.0.0.0:4430"
			// bind-v6 = "[::]:4430"
			max-transfer-short = "100 MiB / min"
			max-transfer-long = "2 TiB / mo"
			max-disk-usage = "16 GiB"
			cache-store = "cache"
			// settings-file = "settings.toml"
			graceful-shutdown = "60 s"
			external-port = 0
			enable-grease = true
			extra-allowed-origins = []
			file-allocation-size = "4 KiB"
		};
		toml.try_into().unwrap()
	};
}

impl Default for Args {
	fn default() -> Self {
		use ByteExp::*;
		use PeriodUnit::*;
		Self {
			client_secret: None,
			server: Server::Staging,
			bind: net::SocketAddrV4::new([0u8; 4].into(), 0u16),
			max_transfer_short: (100, MiB, 1, m).into(),
			max_transfer_long: (2, TiB, 1, M).into(),
			max_disk_usage: (16, GiB).into(),
			cache_store: "cache".into(),
			graceful_shutdown: (60, s).into(),
			external_port: 0.into(),
			enable_grease: false,
			listeners: Listeners::default(),
			staging_data: None,
			extra_allowed_origins: vec![],
			file_allocation_size: (4, KiB).into(),
		}
	}
}
mod parse_server {
	use super::*;
	use serde::{
		de::{
			Deserializer,
			Error as DErr,
		},
		ser::Serializer,
	};
	pub fn from_str(srv: &str) -> Result<Server, Error> {
		srv.parse().map_err(|()| Server::ERROR)
	}
	pub fn serialize<S>(
		srv: &Server,
		s: S,
	) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		s.collect_str(srv)
	}
	pub fn deserialize<'de, D>(d: D) -> Result<Server, D::Error>
	where
		D: Deserializer<'de>,
	{
		let s = String::deserialize(d)?;
		from_str(&s).map_err(DErr::custom)
	}
}
fn parse_atomic_port(prt: &str) -> Result<AtomicU16, Error> {
	Ok(AtomicU16::new(prt.parse()?))
}
#[derive(Debug, Default)]
pub(crate) struct Listeners {
	#[cfg(not(feature = "ipv6"))]
	/// TCP listener (internal)
	pub tcp: Option<net::TcpListener>,
	#[cfg(feature = "ipv6")]
	/// TCP listener V6 (internal)
	pub tcp_v6: Option<net::TcpListener>,
	#[cfg(all(feature = "quic", not(feature = "ipv6")))]
	/// UDP socket (internal)
	pub udp: Option<net::UdpSocket>,
	#[cfg(all(feature = "quic", feature = "ipv6"))]
	/// UDP socket v6 (internal)
	pub udp_v6: Option<net::UdpSocket>,
}
impl Listeners {
	fn listen_tcp<S>(
		&mut self,
		sock: S,
		ext_port: &mut u16,
	) -> Result<net::SocketAddr, Error>
	where
		S: net::ToSocketAddrs,
	{
		loop {
			let l = net::TcpListener::bind(&sock)?;
			match l.local_addr()? {
				net::SocketAddr::V4(addr) => {
					if restricted_ports::is_restricted(addr.port()) {
						// easiest way to pull up a new port
						continue;
					}
					if *ext_port == 0 {
						*ext_port = addr.port();
					}
					l.set_nonblocking(true)?;
					self.tcp = l.into();
					break Ok(addr.into());
				}
				#[cfg(feature = "ipv6")]
				net::SocketAddr::V6(addr) => {
					if restricted_ports::is_restricted(addr.port()) {
						// easiest way to pull up a new port
						continue;
					}
					if *ext_port == 0 {
						*ext_port = addr.port();
					}
					l.set_nonblocking(true)?;
					self.tcp_v6 = l.into();
					break Ok(addr.into());
				}
				#[cfg(not(feature = "ipv6"))]
				v6 => break Err(Error::IPVersion(v6)),
			}
		}
	}

	#[cfg(feature = "quic")]
	fn listen_udp<S>(
		&mut self,
		sock: S,
		ext_port: &mut u16,
	) -> Result<net::SocketAddr, Error>
	where
		S: net::ToSocketAddrs,
	{
		loop {
			let l = net::UdpSocket::bind(&sock)?;
			match l.local_addr()? {
				net::SocketAddr::V4(addr) => {
					if restricted_ports::is_restricted(addr.port()) {
						// easiest way to pull up a new port
						continue;
					}
					if *ext_port == 0 {
						*ext_port = addr.port();
					}
					l.set_nonblocking(true)?;
					self.udp = l.into();
					break Ok(addr.into());
				}
				#[cfg(feature = "ipv6")]
				net::SocketAddr::V6(addr) => {
					if restricted_ports::is_restricted(addr.port()) {
						// easiest way to pull up a new port
						continue;
					}
					if *ext_port == 0 {
						*ext_port = addr.port();
					}
					l.set_nonblocking(true)?;
					self.udp_v6 = l.into();
					break Ok(addr.into());
				}
				#[cfg(not(feature = "ipv6"))]
				v6 => break Err(Error::IPVersion(v6)),
			}
		}
	}

	pub fn tcp(&self) -> net::TcpListener {
		#[cfg(not(feature = "ipv6"))]
		let tcp = self.tcp.take();
		#[cfg(feature = "ipv6")]
		let tcp = self.tcp_v6.take();
		tcp.expect("TCP Listener")
	}
}

pub async fn args(awc: Client) -> Result<Args, Error> {
	const SETTINGS: &'static str = "settings.toml";
	match File::open(SETTINGS) {
		Ok(mut file) => {
			let mut args = String::with_capacity(0x1FFF);
			file.read_to_string(&mut args)?;
			let mut args: Args = toml::from_str(&args)?;
			let ext_port = args.external_port.get_mut();
			if restricted_ports::is_restricted(*ext_port) {
				return Err(Error::Port(*ext_port));
			}
			#[cfg(not(feature = "ipv6"))]
			{
				args.bind = match args.listeners.listen_tcp(args.bind, ext_port)? {
					net::SocketAddr::V4(addr) => addr,
					v6 => return Err(Error::IPVersion(v6)),
				};
			}
			#[cfg(feature = "ipv6")]
			{
				args.bind_v6 =
					match args.listeners.listen_tcp(args.bind_v6, ext_port)? {
						net::SocketAddr::V6(addr) => addr,
						v4 => return Err(Error::IPVersion(v4)),
					};
			}
			#[cfg(feature = "quic")]
			{
				todo!();
			}
			Ok(args)
		}
		Err(e) if e.kind() == io::ErrorKind::NotFound => {
			let mut settings = Args::from_args();
			let _usable = dbg!(usable::usable(&settings.cache_store, true));
			if settings.client_secret.is_none() {
				match settings.server {
					Server::Staging => {
						let mut args = String::with_capacity(0xFF);
						eprintln!("Staging server testing");
						for query in ["User name", "Client name", "Upload speed"].iter() {
							eprint!("{}: ", query);
							io::stdin().read_line(&mut args)?;
						}
						let mut l = args.lines();
						let if_empty = |a: &str, f: &str| -> String {
							let a = a.trim();
							if a.is_empty() { f.into() } else { a.into() }
						};
						let username = if_empty(l.next().expect("username"), "ferris");
						let client_id = if_empty(l.next().expect("client name"), "rusty");
						let speed = if_empty(l.next().expect("speed"), "10 MiB / s")
							.parse()
							.expect("speed parse");
						let data = StagingData {
							user_id: username.clone(),
							username,
							client_id,
							speed,
						};
						println!("{:#?}\n{}", data, serde_json::to_string(&data)?);
						let data: StagingRet = awc
							.post(format!("{}/register", Server::Staging))
							.send_json(&data)
							.await?
							.json()
							.await?;
						println!("{:#?}", data);
					}
					Server::Release => {
						eprint!(
							"Client secret (max {} characters, {} server): ",
							ClientSecret::LENGTH,
							settings.server
						);
						let secret = {
							let mut line = String::with_capacity(ClientSecret::LENGTH + 10);
							io::stdin().read_line(&mut line)?;
							line.trim().parse()?
						};
						settings.client_secret = Some(secret);
					}
				}
			}
			let toml_out = toml::to_string(&settings)? + "\n\n";
			let ok = loop {
				eprintln!("{}\n\nSave to {}? [Y/n]", toml_out, SETTINGS);
				let mut line = String::with_capacity(8);
				io::stdin().read_line(&mut line)?;
				match line.as_bytes()[0].to_ascii_lowercase() {
					b'\n' | b'y' | b't' => break true,
					b'n' => break false,
					_ => continue,
				}
			};
			if ok {
				Open::new()
					.write(true)
					.create_new(true)
					.open(SETTINGS)?
					.write(toml_out.as_bytes())?;
			}
			Ok(settings)
		}
		Err(e) => Err(e.into()),
	}
}

#[derive(Debug, Serialize, Deserialize)]
pub struct StagingData {
	user_id: String,
	username: String,
	client_id: String,
	#[serde(with = "per_sec_trans")]
	speed: TransferPerPeriod,
}
mod per_sec_trans {
	use super::*;
	pub fn serialize<S>(
		speed: &TransferPerPeriod,
		ser: S,
	) -> Result<S::Ok, S::Error>
	where
		S: serde::ser::Serializer,
	{
		ser.serialize_u64(speed.per_sec())
	}
	pub use serde_strz::deserialize;
}
#[derive(Debug, Deserialize)]
pub struct StagingRet {
	secret: ClientSecret,
}
