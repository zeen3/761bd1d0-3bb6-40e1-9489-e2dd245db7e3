use derive_more::{
	Constructor,
	Display,
	From,
};
use enum_utils::*;
use serde::{
	de::{
		self,
		Deserializer,
	},
	ser::Serializer,
	Deserialize,
	Serialize,
};
use std::{
	cmp::Ordering,
	str::FromStr,
};
#[derive(
	Clone,
	Copy,
	Debug,
	Display,
	PartialEq,
	PartialOrd,
	Eq,
	Ord,
	FromStr,
	ReprFrom,
	TryFromRepr,
	Hash,
)]
#[enumeration(case_insensitive)]
#[allow(non_camel_case_types)]
#[repr(u64)]
pub enum ByteExp {
	b = 1,
	Kb = 1_000,
	Kib = 1 << 10,
	Mb = 1_000_000,
	Mib = 1 << 20,
	Gb = 1_000_000_000,
	Gib = 1 << 30,
	Tb = 1_000_000_000_000,
	Tib = 1 << 40,
	Pb = 1_000_000_000_000_000,
	Pib = 1 << 50,
	Eb = 1_000_000_000_000_000_000,
	Eib = 1 << 60,

	B = 8,
	KB = 8_000,
	KiB = 8 << 10,
	MB = 8_000_000,
	MiB = 8 << 20,
	GB = 8_000_000_000,
	GiB = 8 << 30,
	TB = 8_000_000_000_000,
	TiB = 8 << 40,
	PB = 8_000_000_000_000_000,
	PiB = 8 << 50,
	EB = 8_000_000_000_000_000_000,
	EiB = 8 << 60,
}
impl Default for ByteExp {
	fn default() -> Self {
		Self::B
	}
}
#[derive(Clone, Copy, Debug, Display, Default, Constructor, From)]
#[display(fmt = "{} {}", value, unit)]
pub struct Size {
	value: u64,
	unit: ByteExp,
}
macro_rules! mul {
	($n:expr) => {
		&{ $n.value.saturating_mul($n.unit.into()) as u64 }
	};
}
impl Size {
	pub const MAPERR: fn(()) -> super::Error = |()| super::Error::Unit;

	#[allow(bindings_with_variant_name)]
	pub fn to_ref(
		&self,
		b: ByteExp,
	) -> u64 {
		mul!(self).overflowing_div(b.into()).0
	}

	pub fn to_bytes(&self) -> u64 {
		self.to_ref(ByteExp::B)
	}

	pub fn to_kib(&self) -> u64 {
		self.to_ref(ByteExp::KiB)
	}

	pub fn to_mib(&self) -> u64 {
		self.to_ref(ByteExp::KiB)
	}
}
impl std::hash::Hash for Size {
	fn hash<H: std::hash::Hasher>(
		&self,
		state: &mut H,
	) {
		self.to_bytes().hash(state)
	}
}
impl FromStr for Size {
	type Err = super::Error;

	fn from_str(s: &str) -> Result<Self, super::Error> {
		let s: Vec<_> = s.split_ascii_whitespace().take(2).collect();
		match s.as_slice() {
			[value, unit] => {
				let value = value.parse()?;
				let unit = unit.parse().map_err(Self::MAPERR)?;
				Ok(Self { value, unit })
			}
			_ => Err(super::Error::Period),
		}
	}
}
impl PartialEq for Size {
	fn eq(
		&self,
		other: &Self,
	) -> bool {
		mul!(self) == mul!(other)
	}
}
impl PartialOrd for Size {
	fn partial_cmp(
		&self,
		other: &Self,
	) -> Option<Ordering> {
		PartialOrd::partial_cmp(mul!(self), mul!(other))
	}
}
impl Eq for Size {}
impl<'de> Deserialize<'de> for Size {
	fn deserialize<D>(d: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		let s = String::deserialize(d)?;
		s.parse().map_err(de::Error::custom)
	}
}
impl Serialize for Size {
	fn serialize<S>(
		&self,
		s: S,
	) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		s.collect_str(self)
	}
}
impl From<u64> for Size {
	fn from(bytes: u64) -> Self {
		let mut exp = 0u8;
		let (value, unit) = loop {
			if bytes & (1 << exp) - 1 == 0 {
				exp += 10;
			} else {
				break (
					bytes >> exp.saturating_sub(10),
					8u64 * (1 << exp.saturating_sub(10)),
				);
			}
		};
		use std::convert::TryInto;
		let unit = unit.try_into().unwrap();
		Self { value, unit }
	}
}
#[cfg(feature = "libc")]
pub mod usable {
	use super::Size;
	use std::{
		ffi::CString,
		fs,
		io,
		mem::MaybeUninit,
	};
	bitflags::bitflags! {
		pub struct MountFlags: u64 {
				const ST_MANDLOCK = libc::ST_MANDLOCK;
				const ST_NOATIME = libc::ST_NOATIME;
				const ST_NODEV = libc::ST_NODEV;
				const ST_NODIRATIME = libc::ST_NODIRATIME;
				const ST_NOEXEC = libc::ST_NOEXEC;
				const ST_NOSUID = libc::ST_NOSUID;
				const ST_RDONLY = libc::ST_RDONLY;
				const ST_RELATIME = libc::ST_RELATIME;
				const ST_SYNCHRONOUS = libc::ST_SYNCHRONOUS;
			}
	}
	#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
	pub struct Use {
		fs_block_size: Size,
		fragment_size: Size,
		size_in_frags: u64,
		blocks_free: u64,
		nosu_free_blocks: u64,
		inodes: u64,
		free_inodes: u64,
		nosu_free_inodes: u64,
		fs_id: u64,
		mount_flags: MountFlags,
		max_file_len: u64,
	}
	impl From<libc::statvfs64> for Use {
		fn from(stat: libc::statvfs64) -> Self {
			let libc::statvfs64 {
				f_bsize,
				f_frsize,
				f_blocks,
				f_bfree,
				f_bavail,
				f_files,
				f_ffree,
				f_favail,
				f_fsid,
				f_flag,
				f_namemax,
				..
			} = stat;
			Self {
				fs_block_size: f_bsize.into(),
				fragment_size: f_frsize.into(),
				size_in_frags: f_blocks,
				blocks_free: f_bfree,
				nosu_free_blocks: f_bavail,
				inodes: f_files,
				free_inodes: f_ffree,
				nosu_free_inodes: f_favail,
				fs_id: f_fsid,
				mount_flags: MountFlags::from_bits(f_flag).expect("bits"),
				max_file_len: f_namemax,
			}
		}
	}
	pub fn usable<P: AsRef<::std::path::Path>>(
		path: P,
		mk_dir: bool,
	) -> Result<Use, ::std::io::Error> {
		if mk_dir {
			fs::create_dir_all(path.as_ref())?;
		}
		let cst = path
			.as_ref()
			.to_str()
			.ok_or_else(|| io::Error::from(io::ErrorKind::InvalidInput))
			.and_then(|s| {
				CString::new(s)
					.map_err(|_| io::Error::from(io::ErrorKind::InvalidInput))
			})
			.map_err(|e| io::Error::from(e))?;
		let mut vfs = MaybeUninit::<libc::statvfs64>::uninit();
		let result = unsafe { libc::statvfs64(cst.as_ptr(), vfs.as_mut_ptr()) };
		if result == 0 {
			let vfs = unsafe { vfs.assume_init() };
			Ok(vfs.into())
		} else {
			Err(io::Error::last_os_error())
		}
	}
}
