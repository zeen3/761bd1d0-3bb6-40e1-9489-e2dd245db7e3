use derive_more::{
	Constructor,
	Display,
};
use enum_utils::{
	FromStr,
	ReprFrom,
};
use serde::{
	de::{
		self,
		Deserializer,
	},
	ser::Serializer,
	Deserialize,
	Serialize,
};
use std::{
	cmp::Ordering,
	num::NonZeroU64,
	str::FromStr as FromStrCore,
};
#[derive(
	Clone, Copy, Debug, Display, PartialEq, PartialOrd, Eq, Ord, FromStr, ReprFrom,
)]
#[repr(u64)]
#[allow(non_camel_case_types)]
pub enum PeriodUnit {
	#[enumeration(alias = "sec", alias = "second")]
	/// Second
	s = 1,
	#[enumeration(alias = "min", alias = "minute")]
	/// Minute
	m = 60,
	#[enumeration(alias = "hr", alias = "hour")]
	/// Hour
	h = 60 * 60,
	#[enumeration(alias = "day")]
	/// Day (24 hour period, in unix time)
	d = 60 * 60 * 24,
	#[enumeration(alias = "wk", alias = "week")]
	/// Week (168 hour period, in unix time)
	w = 60 * 60 * 24 * 7,
	#[enumeration(alias = "mo", alias = "month")]
	/// Month (365.25/12 day period, in unix time)
	///
	/// This averages the period of a month over a year.
	M = Self::YEAR / 12,
	#[enumeration(alias = "yr", alias = "year")]
	/// Year (365.25 day period, in unix time)
	Y = Self::YEAR,
}
impl PeriodUnit {
	pub const YEAR: u64 = 365 * 24 * 60 * 60 + 60 * 60 * 6;
}
impl Default for PeriodUnit {
	fn default() -> Self {
		Self::s
	}
}
#[derive(Clone, Copy, Debug, Display, Constructor)]
#[display(fmt = "{} {}", period, unit)]
pub struct Period {
	period: NonZeroU64,
	unit: PeriodUnit,
}
impl Period {
	pub fn in_secs(&self) -> u64 {
		self.period.get().saturating_mul(self.unit.into())
	}
}
impl Period {
	pub const DEFAULT_PERIOD: NonZeroU64 =
		unsafe { NonZeroU64::new_unchecked(1) };
	pub const MAPERR: fn(()) -> super::Error = |()| super::Error::Unit;
}
impl Default for Period {
	fn default() -> Self {
		Self {
			period: Self::DEFAULT_PERIOD,
			unit: PeriodUnit::s,
		}
	}
}
impl FromStrCore for Period {
	type Err = super::Error;

	fn from_str(s: &str) -> Result<Self, super::Error> {
		let mut s = s.split_ascii_whitespace();
		let period = s.next().ok_or_else(|| super::Error::Unit)?.parse()?;
		let unit = match s.next() {
			None => PeriodUnit::s,
			Some(unit) => unit.parse().map_err(Period::MAPERR)?,
		};
		match s.next() {
			None | Some("") => Ok(Self { period, unit }),
			_ => Err(super::Error::Unit),
		}
	}
}
impl From<(u64, PeriodUnit)> for Period {
	fn from((time, unit): (u64, PeriodUnit)) -> Self {
		Self {
			period: NonZeroU64::new(time).unwrap_or(Self::DEFAULT_PERIOD),
			unit,
		}
	}
}
impl From<u64> for Period {
	fn from(time: u64) -> Self {
		Self {
			period: NonZeroU64::new(time).unwrap_or(Self::DEFAULT_PERIOD),
			..Self::default()
		}
	}
}
macro_rules! mul {
	($n:expr) => {
		&{ $n.period.get().saturating_mul($n.unit.into()) as u64 }
	};
}
impl PartialEq for Period {
	fn eq(
		&self,
		other: &Self,
	) -> bool {
		mul!(self) == mul!(other)
	}
}
impl PartialOrd for Period {
	fn partial_cmp(
		&self,
		other: &Self,
	) -> Option<Ordering> {
		PartialOrd::partial_cmp(mul!(self), mul!(other))
	}
}
impl<'de> Deserialize<'de> for Period {
	fn deserialize<D>(d: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		let st = String::deserialize(d)?;
		st.parse().map_err(de::Error::custom)
	}
}
impl Serialize for Period {
	fn serialize<S>(
		&self,
		ser: S,
	) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		ser.collect_str(self)
	}
}
