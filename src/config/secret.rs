use super::Error;
use derive_more::{
	AsRef,
	Display,
};
use serde::{
	de::{
		self,
		Deserializer,
		Visitor,
	},
	ser::Serializer,
	Deserialize,
	Serialize,
};
use std::{
	fmt,
	mem::transmute,
	num::NonZeroI8,
	str::FromStr,
};
type Inner = [NonZeroI8; ClientSecret::LENGTH];
type Outer = [u8; ClientSecret::LENGTH];
#[derive(Display, AsRef, Clone, Copy)]
#[display(fmt = "{:?}", self)]
#[as_ref(forward)]
pub struct ClientSecret {
	secret: Inner,
}
impl ClientSecret {
	pub const ERROR: Error = Error::ClientSecret;
	pub const LENGTH: usize = 52;
}
impl fmt::Debug for ClientSecret {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		write!(f, "\"{}\"", "*".repeat(Self::LENGTH / 4))
	}
}
impl FromStr for ClientSecret {
	type Err = Error;

	fn from_str(s: &str) -> Result<Self, Error> {
		if s.len() == Self::LENGTH && s.bytes().all(|c| c.is_ascii_alphanumeric()) {
			let mut secret: Outer = [b'A'; Self::LENGTH];
			let ptr = s.as_bytes();
			secret.copy_from_slice(ptr);
			let secret = unsafe { transmute(secret) };
			Ok(Self { secret })
		} else {
			Err(Self::ERROR)
		}
	}
}
impl Serialize for ClientSecret {
	fn serialize<S>(
		&self,
		s: S,
	) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		let utf: Outer = unsafe { transmute(self.secret) };
		s.serialize_str(unsafe { ::std::str::from_utf8_unchecked(&utf[..]) })
	}
}
struct ClientSecretVisitor;
impl<'de> Visitor<'de> for ClientSecretVisitor {
	type Value = ClientSecret;

	fn expecting(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		fmt::Display::fmt(&ClientSecret::ERROR, f)
	}

	fn visit_str<E>(
		self,
		value: &str,
	) -> Result<Self::Value, E>
	where
		E: de::Error,
	{
		value.parse().map_err(E::custom)
	}
}
impl<'de> Deserialize<'de> for ClientSecret {
	fn deserialize<D>(d: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		d.deserialize_str(ClientSecretVisitor)
	}
}
impl Default for ClientSecret {
	fn default() -> Self {
		Self {
			secret: unsafe { transmute([b'A'; Self::LENGTH]) },
		}
	}
}
