use crate::Error;
use derive_more::{
	Constructor,
	Display,
};
use enum_utils;
use std::str::FromStr;
mod period;
pub mod restricted_ports;
mod secret;
mod size;
pub use period::*;
pub use secret::*;
pub use size::*;
#[cfg_attr(
	feature = "structopt-toml",
	derive(serde::Serialize, serde::Deserialize)
)]
#[derive(Clone, Copy, Debug, Display, enum_utils::FromStr)]
#[enumeration(case_insensitive)]
pub enum Server {
	#[display(fmt = "https://api.mangadex.network")]
	Release,
	#[display(fmt = "https://mangadex-test.net")]
	Staging,
}
impl Server {
	pub const ERROR: Error = Error::ServerType;
}

#[derive(Clone, Copy, Debug, Display, PartialEq, PartialOrd, Constructor)]
#[display(fmt = "{} per {}", transfer, period)]
pub struct TransferPerPeriod {
	transfer: Size,
	period: Period,
}
impl TransferPerPeriod {
	pub fn per_sec(&self) -> u64 {
		self.transfer.to_bytes() / self.period.in_secs()
	}
}
type TPPF = (u64, ByteExp, u64, PeriodUnit);
impl From<TPPF> for TransferPerPeriod {
	fn from((size, sunit, period, tunit): TPPF) -> Self {
		use core::num::NonZeroU64;
		Self {
			transfer: Size::new(size, sunit),
			period: Period::new(
				NonZeroU64::new(period).unwrap_or(Period::DEFAULT_PERIOD),
				tunit,
			),
		}
	}
}
impl FromStr for TransferPerPeriod {
	type Err = Error;

	// TODO: convoluted method, written by use of a mental state map
	// eg "1 per s", "100 MiB per minute", "2 TB / 4 wk", "2 MiB mo"
	fn from_str(st: &str) -> Result<Self, Error> {
		let mut transfer_value = 1;
		let mut transfer_unit = ByteExp::MiB;
		let mut period = Period::DEFAULT_PERIOD;
		let mut period_unit = PeriodUnit::s;
		'word: for (i, word) in st.split_ascii_whitespace().enumerate() {
			match (word, i) {
				("/", 1..=5) | ("per", 1..=5) => continue 'word,
				(num, 0) => transfer_value = num.parse()?,
				(unit, 1) => transfer_unit = unit.parse().map_err(Size::MAPERR)?,
				(num, 2..=4) if num.bytes().all(|by| by.is_ascii_digit()) => {
					period = num.parse()?
				}
				(unit, 2..=4) => period_unit = unit.parse().map_err(Period::MAPERR)?,
				_ => unreachable!(),
			}
		}
		Ok(Self::new(
			Size::new(transfer_value, transfer_unit),
			Period::new(period, period_unit),
		))
	}
}
