use crate::ClientSecret;
use derive_more::{
	Display,
	From,
};
#[derive(Debug, Display, From)]
pub enum Error {
	#[display(
		fmt = "Client secret must be a {} character ascii alphanumeric string",
		ClientSecret::LENGTH
	)]
	ClientSecret,
	#[display(fmt = "Client secret was invalid")]
	InvalidSecret,
	#[display(fmt = "Server type must be one of \"staging\" or \"release\"")]
	ServerType,
	Unit,
	Period,
	#[from(ignore)]
	Port(u16),
	#[from(ignore)]
	Unknown,
	IPVersion(::std::net::SocketAddr),
	Io(::std::io::Error),
	ParseIntError(::core::num::ParseIntError),
	TomlSer(::toml::ser::Error),
	TomlDe(::toml::de::Error),
	ActixClient(::awc::error::HttpError),
	ActixClientSend(::awc::error::SendRequestError),
	ActixClientJson(::awc::error::JsonPayloadError),
	SerdeJson(::serde_json::Error),
	Pinecone(::pinecone::Error),
	#[display(fmt = "Control server failed to acknowledge")]
	ControlServer,
}
impl ::std::error::Error for Error {}
