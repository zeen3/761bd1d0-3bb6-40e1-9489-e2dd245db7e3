use chrono::{
	DateTime,
	Utc,
};
pub type Rfc3339 = DateTime<Utc>;
/// hacky as all fuck
pub mod b64 {
	use base64::{
		decode_config_slice,
		URL_SAFE_NO_PAD,
	};
	struct De<'de>(&'de mut [u8]);
	use std::fmt;
	impl<'de, 'e> Visitor<'e> for De<'de> {
		type Value = usize;

		fn expecting(
			&self,
			f: &mut fmt::Formatter,
		) -> fmt::Result {
			f.write_str("base64")
		}

		fn visit_str<E>(
			self,
			s: &str,
		) -> Result<Self::Value, E>
		where
			E: Error,
		{
			decode_config_slice(s, URL_SAFE_NO_PAD, self.0).map_err(Error::custom)
		}
	}
	use serde::de::*;
	pub fn deserialize<'de, D: Deserializer<'de>>(
		d: D
	) -> Result<Vec<u8>, D::Error> {
		let mut res = vec![0; 64];
		let len = d.deserialize_str(De(&mut res[..]))?;
		res.truncate(len);
		Ok(res)
	}
}
