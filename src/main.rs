#![feature(half_open_range_patterns, exclusive_range_pattern, str_split_once)]
#[cfg(feature = "snmalloc-rs")]
#[cfg_attr(feature = "snmalloc-rs", global_allocator)]
static ALLOC: snmalloc_rs::SnMalloc = snmalloc_rs::SnMalloc;

mod config;
use config::*;
mod error;
pub use error::*;
mod args;
pub use args::*;
mod db;
mod trans;
pub use awc::Client;
mod req;
mod serde_loc;
use actix_web::{
	web::get,
	App,
	HttpServer,
};
pub use serde_loc::*;
pub type DateTime = chrono::DateTime<chrono::Utc>;

#[actix_rt::main]
async fn main() -> Result<(), std::io::Error> {
	dotenv::dotenv().ok();
	let client = awc::Client::build().disable_timeout().finish();
	let args = args::args(client.clone()).await.unwrap();
	let rustls = todo!();
	println!(
		"Mangadex@Home Rust client, version {} initialising (build version {})",
		env!("CARGO_PKG_VERSION"),
		BUILD_VERSION,
	);
	println!(include_str!("../LICENSE"));
	println!("{:#?}", args);

	#[cfg(feature = "lru-disk-cache")]
	let data = db::lru::setup_cache(&args)?;
	HttpServer::new(move || {
		#[cfg(feature = "lru-disk-cache")]
		let (to, data) = (db::lru::serve_file, data.clone());
		App::new().app_data(data).route(get().to(to))
	})
	.listen_rustls(args.listeners.tcp(), rustls)?
	.run()
	.await
}
pub const BUILD_VERSION: u16 = include!("../BUILDVER");
