use derive_more::{
	Display,
	Error,
};
use enum_utils::FromStr;
use std::{
	fmt,
	num::NonZeroU32,
	str,
};
#[derive(Copy, Clone, Hash, PartialEq, PartialOrd, Ord, Eq)]
pub struct Ext {
	inner: ExtInner,
}
impl fmt::Debug for Ext {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		f.write_str(self.as_ext())
	}
}
#[derive(Debug, Display, Error, Clone, Copy)]
pub struct ExtFromStr;
impl str::FromStr for Ext {
	type Err = ExtFromStr;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(Self {
			inner: s.parse().map_err(|()| ExtFromStr)?,
		})
	}
}
mod ext {
	const MASK_SHIFT: u32 = 29;
	pub const MASK: u32 = 0b111 << MASK_SHIFT;
	pub const MASK_SIZE: u32 = !MASK;
	pub const JPG: u32 = 0b001 << MASK_SHIFT;
	pub const GIF: u32 = 0b010 << MASK_SHIFT;
	pub const PNG: u32 = 0b000 << MASK_SHIFT;
	pub const WEBP: u32 = 0b011 << MASK_SHIFT;
	pub const AVIF: u32 = 0b100 << MASK_SHIFT;
	const RESERVED_1: u32 = 0b101 << MASK_SHIFT;
	const RESERVED_2: u32 = 0b110 << MASK_SHIFT;
	const RESERVED_3: u32 = 0b111 << MASK_SHIFT;
}
impl Ext {
	pub fn as_mime_str(&self) -> &'static str {
		use ExtInner::*;
		match self.inner {
			Png => "image/png",
			Jpg => "image/jpeg",
			Gif => "image/gif",
			Webp => "image/webp",
			Avif => "image/avif",
		}
	}

	pub fn as_ext(&self) -> &'static str {
		use ExtInner::*;
		match self.inner {
			Png => "png",
			Jpg => "jpg",
			Gif => "gif",
			Webp => "webp",
			Avif => "avif",
		}
	}

	pub fn from_bits(u: u32) -> (Self, u32) {
		use ext::*;
		use ExtInner::*;
		let this = Self {
			inner: match u & MASK {
				PNG => Png,
				JPG => Jpg,
				GIF => Gif,
				WEBP => Webp,
				AVIF => Avif,
				_ => unreachable!(),
			},
		};
		(this, u & MASK_SIZE)
	}

	pub fn to_bits(
		&self,
		size: u64,
	) -> Result<NonZeroU32, ()> {
		use ext::*;
		use ExtInner::*;
		const MAX_SIZE: u64 = MASK_SIZE as u64;
		if size > MAX_SIZE {
			Err(())
		} else {
			let size: u32 = (size & MAX_SIZE) as u32;
			let high_bits = match self.inner {
				Png => PNG,
				Jpg => JPG,
				Gif => GIF,
				Webp => WEBP,
				Avif => AVIF,
			};
			Ok(unsafe { NonZeroU32::new_unchecked(size | high_bits) })
		}
	}
}

#[derive(FromStr, Debug, Copy, Clone, Hash, PartialEq, PartialOrd, Ord, Eq)]
#[enumeration(rename_all = "lowercase")]
enum ExtInner {
	#[enumeration(alias = "image/png")]
	Png,
	#[enumeration(alias = "jpeg", alias = "image/jpeg")]
	Jpg,
	#[enumeration(alias = "image/gif")]
	Gif,
	#[enumeration(alias = "image/webp")]
	Webp,
	#[enumeration(alias = "image/avif")]
	Avif,
}
