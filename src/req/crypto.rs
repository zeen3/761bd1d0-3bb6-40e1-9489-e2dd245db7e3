const BOX_ZERO_BYTES_LENGTH: usize = 16;
pub fn open<Bx: AsRef<[u8]>, Nonce: AsRef<[u8]>>(bx: Bx, nonce: Nonce) {
  let bx = bx.as_ref();
  let nonce = nonce.as_ref();
  let mut cipher_buffer = vec![0u8; bx.len() + BOX_ZERO_BYTES_LENGTH];
  let message_buffer = cipher_buffer.clone();
  &mut cipher_buffer[BOX_ZERO_BYTES_LENGTH..].copy_from_slice(bx);

  let 
}
