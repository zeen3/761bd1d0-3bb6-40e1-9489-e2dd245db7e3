use super::*;
use derive_more::{
	Constructor,
	Display,
};
use enum_utils::FromStr;
use serde::{
	Deserialize,
	Serialize,
};
use std::{
	fmt,
	num::*,
};
#[cfg(feature = "file-hash")]
pub type FileHash = [u8; 32];

#[derive(
	Clone,
	Copy,
	Debug,
	Deserialize,
	Display,
	FromStr,
	Serialize,
	PartialEq,
	PartialOrd,
	Ord,
	Eq,
	Hash,
)]
#[serde(rename_all = "kebab-case")]
#[enumeration(rename_all= = "kebab-case")]
pub enum DataUse {
	#[display(fmt = "data")]
	Data,
	#[display(fmt = "data-saver")]
	DataSaver,
}
#[derive(Clone, Debug, PartialEq, PartialOrd, Ord, Eq, Hash, Deserialize)]
pub struct Token {
	pub expires: crate::Rfc3339,
	#[serde(with = "hex")]
	pub hash: [u8; 16],
}
#[derive(Clone, Debug, PartialEq, PartialOrd, Ord, Eq, Hash)]
pub struct PageData {
	pub token: Option<Token>,
	pub info: PageInfo,
	_garbage: Option<[(); 4]>,
	_now: crate::DateTime,
	#[cfg(feature = "file-hash")]
	pub(crate) _file_hash: Option<FileHash>,
}
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Ord, Eq, Hash)]
pub struct PageInfo {
	pub size: DataUse,
	pub hash: [u8; 16],
	pub idx: NonZeroU16,
	pub prefix: Prefix,
	pub ext: Ext,
}
impl fmt::Display for PageData {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		if self.token.is_some() {
			f.write_str("/{token}")?
		}
		let &PageInfo {
			size,
			hash,
			idx,
			prefix,
			ext,
		} = &self.info;
		struct HexEnc<T: AsRef<[u8]>>(T);
		impl<T: AsRef<[u8]>> fmt::Display for HexEnc<T> {
			fn fmt(
				&self,
				f: &mut fmt::Formatter,
			) -> fmt::Result {
				const CHARS: [u8; 16] = *b"0123456789abcdef";
				for v in self.0.as_ref().iter() {
					let hi = CHARS[(*v >> 4) as usize];
					let lo = CHARS[(*v & 15) as usize];
					let slic = [hi, lo];
					let s = unsafe { std::str::from_utf8_unchecked(&slic) };
					if let Err(e) = f.write_str(s) {
						return Err(e);
					}
				}
				Ok(())
			}
		}
		let ok = write!(
			f,
			"/{size}/{hash}/{prefix}{idx}.{ext}",
			size = size,
			hash = HexEnc(hash),
			prefix = prefix,
			idx = idx,
			ext = ext.as_ext()
		);
		if self._garbage.is_some() {
			ok?;
			f.write_str("/{garbage}/{garbage}/{garbage}/{garbage}")
		} else {
			ok
		}
	}
}
impl PageData {
	/// Matches /{token}?/{size}/{hash}/{prefix}{idx}(-{_file_hash})?.{ext}/{_garbage:.*}?
	pub fn from_req<S: AsRef<crate::trans::ServerSettings>>(
		path: &str,
		server_settings: S,
	) -> Option<Self> {
		super::parse_req::from_req(path, server_settings).ok()
	}
}
#[derive(Clone, Copy, PartialEq, PartialOrd, Ord, Eq)]
pub struct Page {
	// size: NonZeroU32,
	// ext: Ext,
	ext_size: NonZeroU32,
	#[cfg(feature = "file-hash")]
	file_hash: FileHash,
}
impl Page {
	pub fn new(nz: NonZeroU32) -> Self {
		Page {
			ext_size: nz,
			#[cfg(feature = "file-hash")]
			file_hash: [0; 32],
		}
	}
}
impl fmt::Debug for Page {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		let (ext, size) = Page::to_ext_size(*self);
		let mut s = f
			.debug_struct("Page")
			.field("size", &size)
			.field("ext", &ext);
		#[cfg(feature = "file-hash")]
		let s = s.field("file-hash", &self.file_hash);
		s.finish()
	}
}
impl Page {
	pub fn to_ext_size(self) -> (Ext, u32) {
		Ext::from_bits(self.ext_size.get())
	}

	pub fn to_u32(this: Option<Self>) -> u32 {
		this.map(|nz| nz.ext_size.get()).unwrap_or(0)
	}
}
