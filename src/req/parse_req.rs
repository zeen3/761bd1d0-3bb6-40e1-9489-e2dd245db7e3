use super::*;
use derive_more::*;
use std::convert::TryInto;
#[derive(Debug, Display, Error, Clone, From)]
pub enum FromReqErr {
	MissingUrlSegment,
	InvalidUrlSegment,
	ExcessUrlSegments,
	TokenRequired,
	TokenExpired,
	TokenInvalid,
	JsonInvalid,
	Base64Decode,
	ExtInvalid(ExtFromStr),
	Hex(::hex::FromHexError),
	InvalidPrefix(super::PrefixErr<char>),
	ParseInt(::std::num::ParseIntError),
}
pub fn from_req<S: AsRef<crate::trans::ServerSettings>>(
	path: &str,
	server_settings: S,
) -> Result<PageData, FromReqErr> {
	use FromReqErr::*;
	let _now = chrono::Utc::now();
	let mut path = path.split("/").skip(1);
	let server_settings = server_settings.as_ref();
	let (token, size) = {
		let next = path.next().ok_or(MissingUrlSegment)?;
		if let Ok(size) = next.parse() {
			// if we're seeing big traffic, tokens may be enforced
			if server_settings.force_tokens {
				return Err(TokenRequired);
			}
			(None, size)
		} else {
			let size = path
				.next()
				.ok_or(MissingUrlSegment)?
				.parse()
				.map_err(|()| InvalidUrlSegment)?;
			if next.len() > 220 {
				return Err(TokenInvalid);
			}
			let data = [0u8; 172];
			let len =
				base64::decode_config_slice(next, base64::URL_SAFE_NO_PAD, &mut data)
					.map_err(|_| Base64Decode)?;
			let data = &data[..len];
			use sodiumoxide::crypto::secretbox::xsalsa20poly1305::*;
			let (nonce, key) = server_settings.token_key.split_at(24);
			let nonce = Nonce::from_slice(nonce).unwrap();
			let key = Key::from_slice(key).unwrap();
			let json = open(data, &nonce, &key).map_err(|()| TokenInvalid)?;
			let token: Token =
				serde_json::from_slice(&json).map_err(|_| JsonInvalid)?;
			(Some(token), size)
		}
	};
	// bleh
	let mut hash = [0u8; 16];
	hex::decode_to_slice(path.next().ok_or(MissingUrlSegment)?, &mut hash)?;
	let (prefix, file) = path.next().ok_or(MissingUrlSegment)?.split_at(1);
	let (file, ext) = file.split_once(".").ok_or(InvalidUrlSegment)?;
	let ext = ext.parse()?;
	// compliance for rehosting content en masse: file may have an attached checksum
	let (idx, _file_hash) = {
		if let Some((idx, hash)) = file.split_once("-") {
			let mut slic: FileHash = Default::default();
			hex::decode_to_slice(hash, &mut slic)?;
			(idx.parse()?, Some(slic))
		} else {
			(file.parse()?, None)
		}
	};
	let prefix = prefix.chars().next().ok_or(InvalidUrlSegment)?.try_into()?;
	let _garbage = {
		path
			.next()
			.map(|_g0| {
				// every impl eats 4 pieces of garbage
				let res: Result<[(); 4], FromReqErr> = {
					let _g1 = path.next().ok_or(MissingUrlSegment)?.into();
					let _g2 = path.next().ok_or(MissingUrlSegment)?.into();
					let _g3 = path.next().ok_or(MissingUrlSegment)?.into();
					let _: Option<[&str; 4]> = [_g0.into(), _g1, _g2, _g3].into();
					Ok([(); 4])
				};
				res
			})
			.transpose()?
	};
	// check that there's no excess
	let count = path.count();
	if count != 0 {
		return Err(ExcessUrlSegments);
	}
	if let Some(tk) = token {
		// chapter hash is equal to hash in token, and expiry is less than now
		if tk.hash != hash {
			return Err(TokenInvalid);
		} else if tk.expires < _now {
			return Err(TokenExpired);
		}
	}
	Ok(PageData {
		token,
		info: PageInfo {
			size,
			hash,
			idx,
			prefix,
			ext,
		},
		_garbage,
		_now,
		#[cfg(feature = "file-hash")]
		_file_hash,
	})
}
