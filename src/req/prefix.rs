use derive_more::{
	From,
	Into,
};
use serde::{
	de::{
		self,
		*,
	},
	ser::*,
};
use std::{
	convert::{
		TryFrom,
		TryInto,
	},
	fmt::{
		self,
		Write,
	},
	num::NonZeroU8,
};
#[derive(Clone, Copy, From, Into, PartialEq, PartialOrd, Ord, Eq, Hash)]
pub struct Prefix(NonZeroU8);

impl fmt::Debug for Prefix {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		let ch: char = (*self).into();
		fmt::Debug::fmt(&ch, f)
	}
}
impl fmt::Display for Prefix {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		let ch: char = (*self).into();
		f.write_char(ch)
	}
}

trait PE: Into<char> + Copy {}
impl PE for u8 {}
impl PE for char {}
#[derive(Clone, Copy)]
pub struct PrefixErr<T: PE> {
	ch: T,
}
impl<T: PE> fmt::Display for PrefixErr<T> {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		f.write_str("erronious prefix")?;
		f.write_char(self.ch.into())?;
		f.write_str(", should be an alphabetic ascii character")
	}
}
impl<T: PE> fmt::Debug for PrefixErr<T> {
	fn fmt(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		f.debug_struct("Prefix ")
			.field("char", &self.ch.into())
			.finish()
	}
}
impl<T: PE> ::std::error::Error for PrefixErr<T> {}
impl TryFrom<char> for Prefix {
	type Error = PrefixErr<char>;

	fn try_from(ch: char) -> Result<Self, Self::Error> {
		if ch.is_ascii_alphabetic() {
			let u: u32 = ch.into();
			// SAFETY: this is checked to be A..=Z and a..=z
			let prefix = unsafe { NonZeroU8::new_unchecked(u as u8) };
			Ok(Prefix(prefix))
		} else {
			Err(PrefixErr { ch })
		}
	}
}
impl TryFrom<u8> for Prefix {
	type Error = PrefixErr<u8>;

	fn try_from(ch: u8) -> Result<Self, Self::Error> {
		if ch.is_ascii_alphabetic() {
			// SAFETY: this is checked to be A..=Z and a..=z
			let prefix = unsafe { NonZeroU8::new_unchecked(ch) };
			Ok(Prefix(prefix))
		} else {
			Err(PrefixErr { ch })
		}
	}
}
impl Into<u8> for Prefix {
	fn into(self) -> u8 {
		self.0.get()
	}
}
impl Into<char> for Prefix {
	fn into(self) -> char {
		self.0.get().into()
	}
}
struct PrefixVisitor;
impl<'de> Visitor<'de> for PrefixVisitor {
	type Value = Prefix;

	fn expecting(
		&self,
		f: &mut fmt::Formatter,
	) -> fmt::Result {
		f.write_str("a lowercase ascii char")
	}

	fn visit_char<E: de::Error>(
		self,
		ch: char,
	) -> Result<Prefix, E> {
		ch.try_into().map_err(|e: PrefixErr<char>| {
			let ch: char = e.ch.into();
			de::Error::invalid_value(Unexpected::Char(ch), &self)
		})
	}
}
impl<'de> Deserialize<'de> for Prefix {
	fn deserialize<D: Deserializer<'de>>(d: D) -> Result<Self, D::Error> {
		d.deserialize_char(PrefixVisitor)
	}
}
impl Serialize for Prefix {
	fn serialize<S: Serializer>(
		&self,
		s: S,
	) -> Result<S::Ok, S::Error> {
		s.serialize_char((*self).into())
	}
}
